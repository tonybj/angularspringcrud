(function(angular) {
 
angular.module("myApp").config(function($stateProvider, $urlRouterProvider,$locationProvider) {
	  $urlRouterProvider.otherwise("/");
	  
	  $stateProvider
          // route for the home page
          .state('listState', {
        	  url:"/",
              templateUrl : 'list.html',
              controller  : 'AppController',
              activetab   : 'list'
          })
          
                    // route for the about page
          .state('paginateState', {
              url: '/list/:filter',
              params: {
            	  filter:''
            	  },
        	  templateUrl : 'paginator.html',
              controller  : 'PaginatorController',
               activetab   : 'paginator'
          })

          // route for the about page
          .state('details', {
        	  url: '/details/:id',
              templateUrl : 'details.html',
              controller  : 'AppController'
          });
  });
})(angular);