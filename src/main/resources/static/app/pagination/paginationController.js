(function(angular) {

var MyController = function ($scope,fruitService,Fruit,searchService,$stateParams) {

		  //searchService.loadParameters($stateParams);
		  $scope.searchService = searchService;
		  //console.log("search serv in pgcontr.js"+JSON.stringify(searchService));
		  
		  //2. Caller to REST API
		  var updatePageResult = function(){
			  var pageResponse = fruitService.pageCallService.async(searchService);
			  
			  pageResponse.$promise.then(function(fruitPage){
				  console.log('meals page changed to ' + JSON.stringify(fruitPage));
						$scope.fruitService.items = fruitPage.items;
						$scope.totalMeals = fruitPage.completeSize;
						console.log("num pages"+fruitPage.completeSize)
				  });
		  };
		  
		  $scope.fruitService = fruitService;
		  $scope.totalMeals = fruitService.count;
		  $scope.currentPage = 1;
		  $scope.pageSize = 4;
		  $scope.meals = [];
		  $scope.pageChangeHandler = function(num) {
			updatePageResult();
		    console.log('going to pagez ' + num + ' searchService is num'+searchService.page);
		  };
		  
		    $scope.deleteItem = function(item) {
		    	console.log("item is "+ item.constructor);
		        item.$remove(function() {
		      	  $scope.fruitService.items.splice($scope.fruitService.items.indexOf(item), 1); 
		        });
		      };
		  
		  updatePageResult();
	}


	
	MyController.$inject = ['$scope','fruitService','Fruit','searchService','$stateParams'];
	//angular.module("myApp.controllers").controller('OtherController', OtherController);
	
	angular.module("myApp.controllers").controller('PaginatorController', MyController); 
})(angular);
