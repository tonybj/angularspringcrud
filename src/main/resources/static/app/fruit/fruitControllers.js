//Denna fil deklarerar AppController som hör till modulen myApp.controllers

//Intressant att den har dependencyinjection av Item. 
//En controller har som uppgift att skapa beteende och initiera scope. 
(function(angular) {
	//har en implicit notation som injekterar Item faktory så att man kan skapa Items
  var AppController = function($scope, Fruit,$location,$state,fruitService,Marker,MarkerFunctions) {
	  
	  $scope.fruitService=fruitService;
	  //Laddar in items till $scope för uppvisning
	  //query-metoden finns inne i resource-klassen
	/*Fruit.query(function(response) {
      //$scope.items = response ? response : [];
		$scope.fruitService.items = response ? response : [];
       console.log("in fruitC got "+JSON.stringify());
	});*/
    
	//bounds is of type latlngbound
    var getExtendedBounds = function(bounds,length){
    	
    	var actualBounds = bounds;
    	var newBounds = new google.maps.LatLngBounds(actualBounds.getSouthWest(),actualBounds.getNorthEast());

    	var newSouthWest = new google.maps.LatLng(bounds.getSouthWest().lat()-length,bounds.getSouthWest().lng()-length);
    	var newNorthEast = new google.maps.LatLng(bounds.getNorthEast().lat()+length,bounds.getNorthEast().lng()+length);    	    
    	bounds.extend(newSouthWest);
    	bounds.extend(newNorthEast);
    	return bounds;
    };
    
	$scope.markers=[];
	$scope.map = {
            center: {
                latitude: 42.3349940452867,
                longitude:-71.0353168884369
            },
            zoom: 11,
            markers: [],
            events: {
            click: function (map, eventName, originalEventArgs) {
                var e = originalEventArgs[0];
                var lat = e.latLng.lat(),lon = e.latLng.lng();
    
                var newMark = new Marker({
                	id: null,
                    description: "undescribed",
                    longitude:lon,
                    latitude:lat,
                    name:"olle",
                    checked: false
                    //första argumentet i save är vad som sker vid lyckat resultat, i detta fall spara ner itemet i scope.
                  }).$save(function(response) {
                	  
                      $scope.markers.push(response);
                      console.log("saved new Mark, new id:"+response.id+response.name);
                      	  
                  });
                 
                
            },
            idle: function(map, eventName,originalEventArgs){
            	var actualBounds = map.getBounds();
            	var newBounds = new google.maps.LatLngBounds(actualBounds.getSouthWest(),actualBounds.getNorthEast());
            	var rect = getExtendedBounds(newBounds,2);
            	
            	
            	if($scope.bigBound==null || (!$scope.bigBound.contains(map.getBounds().getSouthWest()) && 
            			!$scope.bigBound.contains(map.getBounds().getSouthWest()))){
            		alert("out of the big box, creating new. Load Markers from db!");
            		
            		//load markers into googleMarker array
            		Marker.query(function(response) {
            		      $scope.markers = response ? response : [];
            		      if($scope.markers){
                  			console.log("found "+$scope.markers.length+" markers");
                  				//$scope.map.markers = $scope.markers;//MarkerFunctions.markersToGoogleMarkers($scope.markers);
                  		}
            		});
            		
            		
			        
            		
            	    $scope.bigBound = getExtendedBounds(map.getBounds(),0.3);
            	}
            	
            	$scope.oldCenter=map.getCenter();
            	
            }
        }
        };
	
      $scope.bigBound = null;
    
      var simpleStringify =function(object){
    	    var simpleObject = {};
    	    for (var prop in object ){
    	        if (!object.hasOwnProperty(prop)){
    	            continue;
    	        }
    	        if (typeof(object[prop]) == 'object'){
    	            continue;
    	        }
    	        if (typeof(object[prop]) == 'function'){
    	            continue;
    	        }
    	        simpleObject[prop] = object[prop];
    	    }
    	    return JSON.stringify(simpleObject); // returns cleaned up JSON
    	};
	
	  $scope.markersEvents = {
			    click: function (gMarker, eventName, model) {
			    	console.log("clicked on Marker #"+gMarker.key);
			    	model.show=!model.show;
			    	/*
			    	new Marker({id:gMarker.key}).$remove();
			    	
			    	//remove marker if it was clicked!
			    	//check coordinates against the marker-list.
			        for (var i = 0; i < $scope.map.markers.length; i++) { 
				    	  if($scope.map.markers[i].id===gMarker.key){  
				    		  $scope.map.markers.splice(i,1);
				    	  }
				    	}
			      */
			      //alert("index:"+"Model: event:" + eventName + " " + simpleStringify(gMarker));
			    }
			  };
    
	
	
	
	
	$scope.editFruit = function (fruitId) {
		$state.go("details",{id:fruitId});
        //$location.path('/details/' + fruitId);
    };
	
	//deklarerar metoder som kan användas från scope
    $scope.addItem = function(description,isChecked) {
      //Skapar ett nytt objekt Item via ItemFactory och använder direkt resource instansmetod $save för att skicka datan till servern.
      new Fruit({
        description: description,
        checked: isChecked
        //första argumentet i save är vad som sker vid lyckat resultat, i detta fall spara ner itemet i scope.
      }).$save(function(item) {
    	  $scope.fruitService.items.push(item);
    	  $scope.fruitService.count++;
      });
      //cleara input-field som är knuten till newItem (är endast beskrivning)
      $scope.newItem = "";
    };
    
    //deklarerar metoder som kan användas från scope.
    $scope.updateItem = function(item) {
      item.$update();
    };
    
    
    $scope.fruits = ["banan","äpple","Päron"];
    
    $scope.deleteItem = function(item) {
      item.$remove(function() {
    	  $scope.fruitService.items.splice($scope.fruitService.items.indexOf(item), 1);
    	  $scope.fruitService.count--;
      });
    };
  };
  
  //lägg till injection dependencies, denna gång via $inject notationen
  AppController.$inject = ['$scope', 'Fruit','$location','$state','fruitService','Marker','MarkerFunctions'];
  angular.module("myApp.controllers").controller("FruitController", AppController);
}(angular));