//En item-factory. För att använda den till något vettigt så 
//behöver man injekta den till en controller. Detta görs genom att
//skapa en funktion enligt function($scope,ItemFactory)


(function(angular) {
	//här är det tydligt att itemFactory skapar konfigurerade resource-objekt. 
  var ItemFactory = function($resource,Fruit) {
    var FruitPage = $resource('/fruits', {
      //plocka page från $resource-objektsanropets parameter
    	//page: '@page'
    }, {
      query: {
          method: "GET",
          transformResponse: function (data, headers) {
              var jsonData = angular.fromJson(data);
        	  jsonData.items = jsonData.items.map(function (item) {
                  return new Fruit(item)
              });
        	  return jsonData; 
        	  
        	  }
        }
    });
    
    return FruitPage;
  };
  
  ItemFactory.$inject = ['$resource','Fruit'];
  //skapar en factory funktion som heter Item (och kan nu skapa klasser med namnet Item(fast som i botten är resourceobjekt skapade via ItemFactory)
  angular.module("myApp.services").factory("FruitPage", ItemFactory);
}(angular));