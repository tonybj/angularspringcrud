(function(angular) {
  
	var Serv = function MyService(FruitPage) {
	    
		//antingen sätta $scope direkt - nackdel: skapar dependency till scope.
		//returnera objektet - klokt
		this.items=[];
		this.count=0;
		/* Hämtar en fruktsida från databasen och returnerar
		 * tillhörande items och totalt antal sidor.
		 * 
		 * param pagenum - vilken sida från databasen
		 * returns - returnerar meals och totalMeals.
		 */
		
		//3. Caller to REST API
		  this.pageCallService = {
				    async: function(searchService) {
				      // $http returns a promise, which has a then function, which also returns a promise
				    	var parameters = searchService.getParameters();
				    	console.log("data in searchservice:"+JSON.stringify(parameters))
				      var promise = FruitPage.query(parameters);
				      // Return the promise to the controller
				      return promise;
				    }
				  };
		  
	};
  
  angular.module("myApp.services").service("fruitService", Serv);
})(angular);