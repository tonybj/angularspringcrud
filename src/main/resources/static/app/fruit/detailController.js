(function(angular) {
	
	angular.module("myApp.controllers").controller("detailedFruitController", 
			['$scope','Fruit','$stateParams', function($scope, Fruit,$stateParams){
		$scope.greeting = "hej från detailController";
		$scope.fruit = Fruit.get({id: $stateParams.id});
	}]);
	
})(angular)