//En item-factory. För att använda den till något vettigt så 
//behöver man injekta den till en controller. Detta görs genom att
//skapa en funktion enligt function($scope,ItemFactory)


(function(angular) {
	//här är det tydligt att itemFactory skapar konfigurerade resource-objekt. 
  var ItemFactory = function($resource) {
    var Fruit = $resource('/fruits/:id', {
      id: '@id'
    }, {
      update: {
        method: "PUT"
      },
      remove: {
        method: "DELETE"
      },
      page: {
    	  isArray: true,
    	  params: {page :0},
          method: "GET",
          transformResponse: function (data, headers) {
        	  return JSON.parse(data).items 
        	  }
        }
    });
    
    
    return Fruit;
  };
  
  ItemFactory.$inject = ['$resource'];
  //skapar en factory funktion som heter Item (och kan nu skapa klasser med namnet Item(fast som i botten är resourceobjekt skapade via ItemFactory)
  angular.module("myApp.services").factory("Fruit", ItemFactory);
}(angular));