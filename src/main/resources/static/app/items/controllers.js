//Denna fil deklarerar AppController som hör till modulen myApp.controllers

//Intressant att den har dependencyinjection av Item. 
//En controller har som uppgift att skapa beteende och initiera scope. 
(function(angular) {
	//har en implicit notation som injekterar Item faktory så att man kan skapa Items
  var AppController = function($scope, Item) {
	  //Laddar in items till $scope för uppvisning
	  //query-metoden finns inne i resource-klassen
	Item.query(function(response) {
      $scope.items = response ? response : [];
    });
    
	//deklarerar metoder som kan användas från scope
    $scope.addItem = function(description) {
      //Skapar ett nytt objekt Item via ItemFactory och använder direkt resource instansmetod $save för att skicka datan till servern.
      new Item({
        description: description,
        checked: false
        //första argumentet i save är vad som sker vid lyckat resultat, i detta fall spara ner itemet i scope.
      }).$save(function(item) {
        $scope.items.push(item);
      });
      //cleara input-field som är knuten till newItem (är endast beskrivning)
      $scope.newItem = "";
    };
    
    //deklarerar metoder som kan användas från scope.
    $scope.updateItem = function(item) {
      item.$update();
    };
    
    $scope.deleteItem = function(item) {
      item.$remove(function() {
        $scope.items.splice($scope.items.indexOf(item), 1);
      });
    };
  };
  
  //lägg till injection dependencies, denna gång via $inject notationen
  AppController.$inject = ['$scope', 'Item'];
  angular.module("myApp.controllers").controller("AppController", AppController);
}(angular));