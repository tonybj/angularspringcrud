(function(angular) {
  
	//1. Used as messenger for calls to REST API
	//stateParams is grabbed from router
	var Serv = function MyService(FruitPage,$stateParams) {
	    
		//antingen sätta $scope direkt - nackdel: skapar dependency till scope.
		//returnera objektet - klokt
		this.itemsPerPage=3;
		this.filter=$stateParams.filter;
		this.page=0;
		
		//Returns all the parameters without the methods
		this.getParameters = function(){
		
			var paraList={};
			for(var propertyName in this) {
				//console.log("type "+this[propertyName]+" is "+ typeof this[propertyName])
				   if(typeof this[propertyName]!="function" ){
					   paraList[propertyName]=this[propertyName];
				   }
				}
			return paraList;
		};
		
		this.loadParameters = function(params){
			console.log("entering parameter Load: "+JSON.stringify(params));
			for (var attrname in params) { this[attrname] = params[attrname]; }

		};
		/* Hämtar en fruktsida från databasen och returnerar
		 * tillhörande items och totalt antal sidor.
		 * 
		 * param pagenum - vilken sida från databasen
		 * returns - returnerar meals och totalMeals.
		 */
		  
	};
  
  angular.module("myApp.services").service("searchService", Serv);
})(angular);