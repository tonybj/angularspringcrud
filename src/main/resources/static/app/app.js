//Säger att dessa moduler ska skapas
//Här skapas myApp och myApp.controllers och services.
//Allting placeras som dependencies i myApp.

//denna fil ska laddas in först så att controllers och services finns tillgängliga

(function(angular) {
  angular.module("myApp.controllers", []);
  angular.module("myApp.services", []);
  angular.module("myApp", ["ui.router","ngResource", "myApp.controllers", "myApp.services",'uiGmapgoogle-maps','angularUtils.directives.dirPagination']).config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyAJQesrVKszk1QD7EW-rKDfouYPfr2aBSc',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
});
  
}(angular));

