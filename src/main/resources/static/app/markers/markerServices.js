//En item-factory. För att använda den till något vettigt så 
//behöver man injekta den till en controller. Detta görs genom att
//skapa en funktion enligt function($scope,ItemFactory)


(function(angular) {
	//här är det tydligt att itemFactory skapar konfigurerade resource-objekt. 
  var MarkerFactory = function($resource) {
    return $resource('/markers/:id', {
      id: '@id'
    }, {
      update: {
        method: "PUT"
      },
      remove: {
        method: "DELETE"
      }
    });
  };
  
  var MarkerFunctions = function() {
	  
	  this.markersToGoogleMarkers = function(markers){
		  
		  googMarks=[];
	      for (var i = 0; i < markers.length; i++) { 
	        	var newGMark = {id:markers[i].id, idkey:markers[i].id , coords: {latitude: markers[i].latitude, longitude:markers[i].longitude} };
	        	googMarks.push(newGMark);
	      }
	        
		  console.log("marker services");
		    
		  return googMarks; 
		    
	  };
	  
  }
  
  MarkerFactory.$inject = ['$resource'];

  //skapar en factory funktion som heter Item (och kan nu skapa klasser med namnet Item(fast som i botten är resourceobjekt skapade via ItemFactory)
  angular.module("myApp.services").factory("Marker", MarkerFactory);
  angular.module("myApp.services").service("MarkerFunctions",MarkerFunctions);
}(angular));