package se.marinel.markers;

import javax.persistence.*;


@Entity
public class Marker {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer id;
  
  @Column
  double latitude;
  @Column
  double longitude;
  @Column
  String name;
  @Column
  private boolean checked;
  @Column
  private String description;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public boolean isChecked() {
    return checked;
  }

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public Double getLongitude() {
	    return longitude;
	  }

  public void setLongitude(double lng) {
	    this.longitude = lng;
	  }
  
  public Double getLatitude() {
	    return latitude;
	  }

	  public void setLatitude(double lat) {
	    this.latitude = lat;
	  }
  
  public String getName() {
	    return name;
	  }

	  public void setName(String description) {
	    this.name = description;
	  }
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
