package se.marinel.fruits;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import be.g00glen00b.controller.ListPage;
import se.marinel.items.Item;
import se.marinel.items.ItemRepository;



@RestController
@RequestMapping("/fruits")
public class FruitController {
  @Autowired
  private FruitRepository repo;
  
  /*
  @RequestMapping(method = RequestMethod.GET)
  public List<Fruit> findItems() {
    return repo.findAll();
  }
  */
  
  //f�r in JSON {description:"banan",checked:true} tex mappas in i ett Fruit item.
  @RequestMapping(method = RequestMethod.POST)
  public Fruit addItem(@RequestBody Fruit item) {
    item.setId(null);
    return repo.saveAndFlush(item);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public Fruit updateItem(@RequestBody Fruit updatedItem, @PathVariable Integer id) {
    updatedItem.setId(id);
    return repo.saveAndFlush(updatedItem);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Fruit getItem(@PathVariable Integer id) {
	  System.out.println("Got request for:"+id);
    Fruit fruit = repo.findOne(id); 
		if(fruit!=null)
    return fruit;
		else
	return null;
  }
  
  //genom att ha params s� kan man g�ra att requests med en viss aprameter kan mappas till just denna "controller".
  //i detta fall �r detta page parametern.
  /**
   * REST interface responding to GET for /fruits/<pagenumber>?page
   * returns a list of all fruits in a paginated way.
   * 
   * @param pageNumber the page number, which holds N pages
   * @return
   */
  @RequestMapping(method = RequestMethod.GET)
  public ListPage<Fruit> getPage(@RequestParam Map<String,String> allParams) {
	    int pageNumber=0;
	    int itemsPerPage=4;
	    if(allParams.get("page")!=null)
		  pageNumber= Integer.parseInt(allParams.get("page"));
	  
		  if(allParams.get("itemsPerPage")!=null)
			  itemsPerPage= Integer.parseInt(allParams.get("itemsPerPage"));
		    

	    
	  	if(pageNumber<1)
	  		pageNumber=1;
	  	
	    //final int PAGE_SIZE = 4;
	    
	    ListPage<Fruit> listPage = new ListPage<Fruit>();
	    
	  //System.out.println("Got request for page:"+allRequestParams.toString());

	  PageRequest request =
	            new PageRequest(pageNumber - 1, itemsPerPage, Sort.Direction.DESC, "id");
	  
    List<Fruit> fruits = repo.findAll(request).getContent(); 
	Long numOfFruits = repo.count();
    
    listPage.setItems(fruits);
    listPage.setCompleteSize(numOfFruits);
    
    System.out.println(listPage);
    
	return listPage;
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deleteItem(@PathVariable Integer id) {
    repo.delete(id);
  }
}
