package se.marinel.fruits;

import org.springframework.data.jpa.repository.JpaRepository;

import se.marinel.items.Item;

public interface FruitRepository extends JpaRepository<Fruit, Integer> {

}