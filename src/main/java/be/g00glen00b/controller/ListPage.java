package be.g00glen00b.controller;

import java.util.ArrayList;
import java.util.List;

public class ListPage<T> {

	private List<T> items = new ArrayList<T>();
	private Long completeSize;
	
	public List<T> getItems() {
		return items;
	}
	public void setItems(List<T> items) {
		if(items==null)
			return;
		this.items.clear();
		this.items.addAll(items);
	}
	public Long getCompleteSize() {
		return completeSize;
	}
	public void setCompleteSize(Long completeSize) {
		this.completeSize = completeSize;
	}
	
	public String toString(){
		
		if(items==null)
			return "item is null";
		
		return items.size()+" total length:"+completeSize;
	}
}
